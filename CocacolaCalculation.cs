public class CocacolaCalculation
    {
        private long NumOfExchange { get; set; }

        private long NumOfIncrease { get; set; }

        public CocacolaCalculation(long numberOfExchange, long numOfIncrease)
        {
            this.NumOfExchange = numberOfExchange;
            this.NumOfIncrease = numOfIncrease;
        }

        public void ExchangeItem()
        {
            long num;
            Console.WriteLine("Nhập số lon bạn đang có: ");
            string str = Console.ReadLine();
            while(!long.TryParse(str, out num) || num <= 0 || num >= Math.Pow(10, 5))
            {
                Console.WriteLine("Số lon phải là số nguyên thỏa mãn 0 < n < 100000, vui lòng nhập lại: ");
                str = Console.ReadLine();
            }
            long total = num + TotalReceive(num);
            Console.WriteLine("Số lon bạn có thể uống là: {0} lon", total);
            Console.ReadLine();
        }

        public long TotalReceive(long totalExchange)
        {
            long totalReceive = (long)Math.Floor((decimal)totalExchange / NumOfExchange) * NumOfIncrease;
            if (totalReceive == 0)
                return 0;

            long currentTotal = totalReceive + totalExchange % NumOfExchange;
            return totalReceive + TotalReceive(currentTotal);
        }
    }
